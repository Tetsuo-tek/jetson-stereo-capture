#include "jetsonstereocapture.h"
#include <Core/Containers/skarraycast.h>

#if defined(ENABLE_CUDA)

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(JetsonStereoCapture, SkFlowSat)
{
    vc0 = nullptr;
    vc1 = nullptr;

    f0Publisher = nullptr;
    f1Publisher = nullptr;
    stereoPublisher = nullptr;
    anaglyphPublisher = nullptr;
    disparityPublisher = nullptr;

    matchMeth = ST_NO_MATCH;
    ndisp = 64;

    compression = 0;

    props.apiReference = VideoCaptureAPIs::CAP_GSTREAMER;
    props.enabled = false;
    props.realTimeMode = false;

    setObjectName("JetsonStereoCapture");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool JetsonStereoCapture::onSetup()
{
    SkCli *cli = skApp->appCli();

    props.fps =  cli->value("--fps").toInt();
    compression = cli->value("--compression").toInt();

    if (!parseSize(objectName(), cli->value("--resolution").data(), &props.resolution))
        return false;

    props.forceFps = cli->isUsed("--force-fps");
    props.forceResolution = cli->isUsed("--force-resolution");

    matchMeth = stringToMethod(cli->value("--stereo-match-method").data());

    AssertKiller(matchMeth == ST_NO_MATCH);

    f0Publisher = new SkFlowVideoPublisher(this);
    f0Publisher->setObjectName(this, "Frame0Publisher");

    f1Publisher = new SkFlowVideoPublisher(this);
    f1Publisher->setObjectName(this, "Frame1Publisher");

    stereoPublisher = new SkFlowVideoPublisher(this);
    stereoPublisher->setObjectName(this, "StereoPublisher");

    anaglyphPublisher = new SkFlowVideoPublisher(this);
    anaglyphPublisher->setObjectName(this, "AnaglyphPublisher");

    disparityPublisher = new SkFlowVideoMJpegPublisher(this);
    disparityPublisher->setObjectName(this, "DisparityPublisher");

    setupPublisher(f0Publisher);
    setupPublisher(f1Publisher);
    setupPublisher(stereoPublisher);
    setupPublisher(anaglyphPublisher);
    setupPublisher(disparityPublisher);

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void JetsonStereoCapture::onInit()
{
    SkString pipeline = buildGstPipelineString(0);
    vc0 = new SkImageCapture(this);
    vc0->setObjectName(this, "ImageCapture_0");
    vc0->setup(pipeline.c_str(), props);

    pipeline = buildGstPipelineString(1);
    vc1 = new SkImageCapture(this);
    vc1->setObjectName(this, "ImageCapture_1");
    vc1->setup(pipeline.c_str(), props);

    AssertKiller(!vc0->open() || !vc1->open());

    props.fps = vc0->getFps();
    props.resolution = vc0->getResolution();

    f0.set(Mat(props.resolution, CV_8UC3, Scalar(0, 0, 0)));
    f1.set(Mat(props.resolution, CV_8UC3, Scalar(0, 0, 0)));
    anaGlyph.set(Mat(props.resolution, CV_8UC3, Scalar(0, 0, 0)));

    f0Publisher->setup(&f0, props.resolution, props.fps, compression, "F0");
    f1Publisher->setup(&f1, props.resolution, props.fps, compression, "F1");
    anaglyphPublisher->setup(&anaGlyph, props.resolution, props.fps, compression, "ANAGLYPH");
    disparityPublisher->setup(&disparity, props.resolution, props.fps, compression, "DISPARITY");

    f0Region = Rect(0, 0, props.resolution.width, props.resolution.height);
    f1Region = Rect(props.resolution.width, 0, props.resolution.width, props.resolution.height);

    if (matchMeth == ST_BM)
        bm = cuda::createStereoBM(ndisp);

    else if (matchMeth == ST_BP)
        bp = cuda::createStereoBeliefPropagation(ndisp);

    else if (matchMeth == ST_CSBP)
        csbp = cv::cuda::createStereoConstantSpaceBP(ndisp);

    disparity.set(Mat(props.resolution, CV_8U, Scalar(0, 0, 0)));
    gpuDisp = cuda::GpuMat(props.resolution, CV_8U);

    Size stereoRes = props.resolution;
    stereoRes.width *= 2;

    stereoFrame.set(Mat(stereoRes, CV_8UC3, Scalar(0, 0, 0)));
    stereoPublisher->setup(&stereoFrame, stereoRes, props.fps, compression, "STEREO");

    f0Publisher->start();
    f1Publisher->start();
    anaglyphPublisher->start();
    stereoPublisher->start();
    disparityPublisher->start();

    if (props.forceFps)
        skApp->changeFastZone(vc0->getTickTimeMicros());

    ObjectMessage("Capture STARTED"
                  << " - res: " << stereoRes
                  << " - fps: " << props.fps << " [" << vc0->getTickTimeMicros() << " us]"
                  << " - frameRawSize: " << stereoPublisher->getRawFrameSize() << " B");

    //IT WILL CHANGE TO FREELOOP WHEN ONE TARGET WILL EXIST AT LEAST
    eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);
}

void JetsonStereoCapture::onQuit()
{
    f0Publisher->stop();
    f1Publisher->stop();
    anaglyphPublisher->stop();
    stereoPublisher->stop();
    disparityPublisher->stop();

    ObjectMessage("Capture STOPPED");

    if (vc0->isOpen())
    {
        vc0->close();
        vc1->close();
    }

    vc0->destroyLater();
    vc0 = nullptr;

    vc1->destroyLater();
    vc1 = nullptr;

    props.enabled = false;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool JetsonStereoCapture::hasTargets()
{
    return f0Publisher->hasTargets()
           || f1Publisher->hasTargets()
           || stereoPublisher->hasTargets()
           || anaglyphPublisher->hasTargets()
           || disparityPublisher->hasTargets();
}

bool JetsonStereoCapture::activityCheck()
{
    if (!vc0/* || !vc0->isOpen()*/)
        return false;

    if (props.enabled)
    {
        if (!hasTargets())
        {
            ObjectMessage("Capture tick NOT-ACTIVE");

            props.enabled = false;
            vc0->setEnabled(false);
            vc1->setEnabled(false);

            eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

            return false;
        }
    }

    else
    {
        if (hasTargets())
        {
            ObjectMessage("Capture tick ACTIVE");

            props.enabled = true;
            vc0->setEnabled(true);
            vc1->setEnabled(true);

            if (props.forceFps)
                skApp->changeTimerMode(SK_TIMEDLOOP_RT);

            else
                skApp->changeTimerMode(SK_FREELOOP);
        }

        else
            return false;
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*
    cameraMatrix1: [3×3 double]
      distCoeffs1: [-0.2745 -0.0184 0 0 0 0 0 -0.2443 0 0 0 0 0 0]
    cameraMatrix2: [3×3 double]
      distCoeffs2: [-0.2808 0.0932 0 0 0 0 0 0.0167 0 0 0 0 0 0]
                R: [3×3 double]
                T: [3×1 double]
                E: [3×3 double]
                F: [3×3 double]
        reprojErr: 0.4828
*/

void JetsonStereoCapture::onFastTick()
{
    if (!activityCheck())
        return;

    f0.set(vc0->getFrame());
    f1.set(vc1->getFrame());

    if (f0.isEmpty() || f1.isEmpty())
        return;

    f0Publisher->tick();
    f1Publisher->tick();

    if (stereoPublisher->hasTargets())
    {
        f0.get().copyTo(stereoFrame.get()(f0Region));
        f1.get().copyTo(stereoFrame.get()(f1Region));

        stereoPublisher->tick();
    }

    if (anaglyphPublisher->hasTargets())
    {
        vector<Mat> bgr0;
        vector<Mat> bgr1;

        f0.splitToChannels(bgr0);
        f1.splitToChannels(bgr1);

        vector<Mat> bgrAnaglyph;
        bgrAnaglyph.push_back(bgr0[0]);
        bgrAnaglyph.push_back(bgr0[1]);
        bgrAnaglyph.push_back(bgr1[2]);

        anaGlyph.mergeFromChannels(bgrAnaglyph);

        anaglyphPublisher->tick();
    }

    if (disparityPublisher->hasTargets())
    {
        if (matchMeth == StereoMatchMethod::ST_BM)
        {
            f0.toGrayScale();
            f1.toGrayScale();

            d0.upload(f0.get());
            d1.upload(f1.get());

            bm->compute(d0, d1, gpuDisp);
        }

        else
        {
            d0.upload(f0.get());
            d1.upload(f1.get());

            if (matchMeth == ST_BP)
                bp->compute(d0, d1, gpuDisp);

            else if (matchMeth == ST_CSBP)
                csbp->compute(d0, d1, gpuDisp);
        }

        gpuDisp.download(disparity.get());
        disparityPublisher->tick();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkString JetsonStereoCapture::buildGstPipelineString(int sensorID)
{
    SkString pipeline("nvarguscamerasrc sensor-id=");
    pipeline.concat(sensorID);
    pipeline.append(" ! video/x-raw(memory:NVMM), width=(int)");
    pipeline.concat(props.resolution.width);
    pipeline.append(", height=(int)");
    pipeline.concat(props.resolution.height);
    pipeline.append(", framerate=(fraction)");
    pipeline.concat(props.fps);
    pipeline.append("/1 ! nvvidconv flip-method=");
    pipeline.concat(2);
    pipeline.append(" ! video/x-raw, width=(int)");
    pipeline.concat(props.resolution.width);
    pipeline.append(", height=(int)");
    pipeline.concat(props.resolution.height);
    pipeline.append(", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink");

    return pipeline;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

CStr *JetsonStereoCapture::methodToString(StereoMatchMethod m)
{
    if (m == StereoMatchMethod::ST_BM)
        return "BM";

    else if (m == StereoMatchMethod::ST_BP)
        return "BP";

    else if (m == StereoMatchMethod::ST_CSBP)
        return "CSBP";

    return "NO_MATCH";
}

StereoMatchMethod JetsonStereoCapture::stringToMethod(CStr *m)
{
    if (SkString::compare(m, "BM"))
        return ST_BM;

    else if (SkString::compare(m, "BP"))
        return ST_BP;

    else if (SkString::compare(m, "CSBP"))
        return ST_CSBP;

    return ST_NO_MATCH;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif
