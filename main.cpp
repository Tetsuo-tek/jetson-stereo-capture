#include "jetsonstereocapture.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--resolution",                "-r", "1280x720",   "Setup the frame resolution");
    cli->add("--force-resolution",          "",   "",           "Use selected resolution when this value is NOT supported by device");
    cli->add("--compression",               "-c", "70",         "Setup the jpeg-compression [1, 100]");
    cli->add("--fps",                       "-f", "10",         "Setup the frames per second value");
    cli->add("--force-fps",                 "",   "",           "Use selected fps when this value is NOT supported by device");
    cli->add("--stereo-match-method",       "-d", "BM",         "Setup the match algorithm to make stereo disparity map");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(10000, 150000, SK_FREELOOP);
    new JetsonStereoCapture;

    return skApp->exec();
}
