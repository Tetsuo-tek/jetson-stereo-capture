#ifndef JETSONSTEREOCAPTURE_H
#define JETSONSTEREOCAPTURE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#if defined(ENABLE_CUDA)

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include "Core/System/Network/FlowNetwork/skflowvideopublisher.h"
#include <Multimedia/Image/skimagecapture.h>
#include <Core/System/skcli.h>
#include <opencv2/cudastereo.hpp>

//JETSON NANO CAM
//--gstreamer-source "nvarguscamerasrc sensor-id=0 ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, framerate=(fraction)30/1
//                  ! nvvidconv flip-method=2 ! video/x-raw, width=(int)1280, height=(int)720, format=(string)BGRx
//                  ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum StereoMatchMethod
{
    ST_NO_MATCH,
    ST_BM,
    ST_BP,
    ST_CSBP
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class JetsonStereoCapture extends SkFlowSat
{
    SkImageCapture *vc0;
    SkMat f0;
    Rect f0Region;

    SkImageCapture *vc1;
    SkMat f1;
    Rect f1Region;

    SkMat stereoFrame;

    StereoMatchMethod matchMeth;
    SkMat disparity;
    cuda::GpuMat d0;
    cuda::GpuMat d1;
    cuda::GpuMat gpuDisp;
    int ndisp; // Max disparity + 1
    Ptr<cuda::StereoBM> bm;
    Ptr<cuda::StereoBeliefPropagation> bp;
    Ptr<cuda::StereoConstantSpaceBP> csbp;

    SkMat anaGlyph;

    int compression;
    SkVideoCaptureProperties props;

    SkFlowVideoPublisher *f0Publisher;
    SkFlowVideoPublisher *f1Publisher;
    SkFlowVideoPublisher *stereoPublisher;
    SkFlowVideoMJpegPublisher *disparityPublisher;
    SkFlowVideoPublisher *anaglyphPublisher;

    public:
        Constructor(JetsonStereoCapture, SkFlowSat);

    private:
        bool onSetup()              override;
        void onInit()               override;
        void onQuit()               override;

        void onFastTick()           override;

        bool hasTargets();
        bool activityCheck();

        SkString buildGstPipelineString(int sensorID);

        static CStr *methodToString(StereoMatchMethod m);
        static StereoMatchMethod stringToMethod(CStr *m);
};

#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // JETSONSTEREOCAPTURE_H
